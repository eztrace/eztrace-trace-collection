# MPI + OpenMP traces

Below are several MPI+OpenMP traces running on [Bran](../../hardware/bran.md)

## Software setting

- MPI vendor: [MPICH](https://www.mpich.org/) version 4.0.2
- Compiler: clang-14
- EZTrace plugins: `mpi ompt`

## Traces

For each trace, we provide a tarball containing the OTF2 trace
([bz2]), as well as the log of the application when run with EZTrace
([log EZTrace]) and without EZTrace ([log vanilla])

- miniFE [[trace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE.x_trace.tar.bz2) (3.9MB)] [[log vanilla](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE_eztrace.log)] [[log EZTrace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE_vanilla.log)]


- miniFE (large) [[trace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE.x_large_trace.tar.bz2) (3.8MB)] [[log vanilla](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE_large_eztrace.log)] [[log EZTrace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/miniFE_large_vanilla.log)]

- Quicksilver [[trace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/qs_trace.tar.bz2) (113MB)] [[log vanilla](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/qs_eztrace.log)] [[log EZTrace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/qs_vanilla.log)]

- AMG [[trace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/amg_300_trace.tar.bz2) (163MB)] [[log vanilla](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/amg_300_eztrace.log)] [[log EZTrace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/amg_300_vanilla.log)]

- Lulesh [[trace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/lulesh2.0_s10_trace.tar.bz2) (3.7GB)] [[log vanilla](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/lulesh2.0_s10_eztrace.log)] [[log EZTrace](https://stark2.int-evry.fr/traces/eztrace-2.0/MPI+OpenMP/lulesh2.0_s10_vanilla.log)]
