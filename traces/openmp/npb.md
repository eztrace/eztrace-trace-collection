# NAS Parallel Benchmarks

Below are several traces of the NAS Parallel Benchmarks running on [Sandor](../../hardware/sandor.md)

## Software setting

- Application: [NAS Parallel Benchmarks](https://www.nas.nasa.gov/software/npb.html) version 3.3.1, OpenMP implementation
- OpenMP implementation: [GNU OpenMP](https://gcc.gnu.org/) version 11.3
- EZTrace plugins: `openmp`

## Traces

For each trace, we provide a tarball containing the OTF2
trace(`*_trace.tar.bz2`), as well as the log of the application when
run with EZTrace (`*_eztrace.log`) and without EZTrace
(`*_vanilla.log`).


[All the traces](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP.tar.bz2) (2.8 GB)

| Kernel | Problem sizes | Number of threads         | Download                                                              |
|--------|---------------|---------------------------|-----------------------------------------------------------------------|
| BT     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| CG     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| DC     | S, A          | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| EP     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| FT     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| LU     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| MG     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| SP     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |
| UA     | S, A, B, C    | 1, 4, 8, 16, 32, 64, 128* | [url](https://stark2.int-evry.fr/traces/eztrace-2.0/NPB3.3.1-OpenMP/) |

## Note

We bind the OpenMP threads by setting the `GOMP_CPU_AFFINITY` environment variable as follows:
```
export GOMP_CPU_AFFINITY=`hwloc-calc --physical-output --intersect PU --no-smt all`
```

When running with 128 threads, we enable SMT by setting `GOMP_CPU_AFFINITY` to:
```
export GOMP_CPU_AFFINITY=`hwloc-calc --physical-output --intersect PU all`
```
