# Bran

## Overview

Bran is a 48 cores/96 threads machine. It is equipped with 128GB of NVRAM, and 2 kinds of disk.

## Hardware
* 2 [Intel(R) Xeon(R) Gold 5220R](https://en.wikichip.org/wiki/intel/xeon_gold/5220r) CPU @ 2.20GHz (24 cores / 48 thread)
* 192 GB RAM
* 128 GB NVRAM
* 960 NVME
* 1.8TB SAS

![alt text](bran.png  "Topology")
