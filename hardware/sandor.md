# Sandor

## Hardware setting

- 2 AMD EPYC 7502 CPUs ( 32 cores / 64 threads each)
- 512GB RAM
- 4TB SAS disc (system)
- GPU Nvidia Quadro RTX 5000

![alt text](sandor.png  "Topology")