# EZTrace trace collection

The EZTrace trace collection contains traces of HPC applications to the research community for analysis.

## Traces

### MPI traces
- [NAS Parallel Benchmarks](traces/mpi/npb.md)

### OpenMP traces
- [NAS Parallel Benchmarks](traces/openmp/npb.md)

### MPI+OpenMP traces
- [MPI+OpenMP traces](traces/mpi_openmp/traces.md)

## Hardware setting

The traces were collected by running applications on several machines or clusters, including:
- [Sandor](hardware/sandor.md)
- [Bran](hardware/bran.md)

## Software

- Tracing tool: [EZTrace](https://eztrace.gitlab.io/eztrace/).
- Trace format: [OTF2](https://www.vi-hps.org/projects/score-p/).
- Visualizing the traces: [ViTE](https://solverstack.gitlabpages.inria.fr/vite/)
- Analyzing the traces: [EasyTraceAnalyzer](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer)

## Contact

- [François Trahay](mailto:francois.trahay@telecom-sudparis)
- [Parallel & Distributed Systems group](https://www.inf.telecom-sudparis.eu/pds/)  at [Télécom SudParis](https://www.telecom-sudparis.eu/) / [Institut Polytechnique de Paris](http://cs.ip-paris.fr/)
